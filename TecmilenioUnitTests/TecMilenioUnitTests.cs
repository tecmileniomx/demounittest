﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TecMilenioDemoProject;

namespace TecmilenioUnitTests
{
    [TestClass]
    public class TecMilenioUnitTests
    {
        [TestMethod]
        public void ValidaPerimetroCirculo()
        {
            var circulo = new Circulo(2.5);
            Assert.AreEqual(circulo.CalculaPerimetro(), 15.70, .1);
        }
        [TestMethod]
        public void ValidaAreaCirculo()
        {
            var circulo = new Circulo(2.5);
            Assert.AreEqual(circulo.CalculaArea(), 19.63, .1);
        }
        [TestMethod]
        public void ValidaPerimetroRectangulo()
        {
            var rectangulo = new Rectangulo(2, 4);
            Assert.AreEqual(rectangulo.CalculaPerimetro(), 12, .1);
        }
        [TestMethod]
        public void ValidaAreaRectangulo()
        {
            var rectangulo = new Rectangulo(2, 4);
            Assert.AreEqual(rectangulo.CalculaArea(), 8, .1);
        }
        [TestMethod]
        public void ValidaPerimetroCuadrado()
        {
            var cuadrado = new Cuadrado(2.5);
            Assert.AreEqual(cuadrado.CalculaPerimetro(), 10, .1);
        }
        [TestMethod]
        public void ValidaAreaCuadrado()
        {
            var cuadrado = new Cuadrado(3);
            Assert.AreEqual(cuadrado.CalculaArea(), 9, .1);
        }

        [TestMethod]
        public void ValidaTrianguloEsIFiguraGeometrica()
        {
            var obj = new Triangulo(1, 4, 2);
            Assert.IsNotInstanceOfType(obj, typeof(IFiguraGeometrica));
        }
        [TestMethod]
        public void ValidaCirculoEsIFiguraGeometrica()
        {
            var obj = new Circulo(5);
            Assert.IsInstanceOfType(obj, typeof(IFiguraGeometrica));
        }
    }
}
