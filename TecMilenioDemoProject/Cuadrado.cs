﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecMilenioDemoProject
{
    public class Cuadrado : IFiguraGeometrica
    {
        private readonly double _lado;

        public Cuadrado(double lado)
        {
            _lado = lado;
        }

        public double CalculaPerimetro()
        {
            return 4*_lado;
        }

        public double CalculaArea()
        {
            return Math.Pow(_lado, 2);
        }
    }
}
