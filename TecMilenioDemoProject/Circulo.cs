﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecMilenioDemoProject
{
    public class Circulo : IFiguraGeometrica
    {
        private readonly double _radio;

        public Circulo(double radio)
        {
            _radio = radio;
        }

        public double CalculaPerimetro()
        {
            return 2*Math.PI*_radio;
        }

        public double CalculaArea()
        {
            return Math.PI*Math.Pow(_radio, 2);
        }
    }
}
