﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecMilenioDemoProject
{
    public interface IFiguraGeometrica
    {
        double CalculaPerimetro();
        double CalculaArea();
    }
}
