﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecMilenioDemoProject
{
    public class Rectangulo : IFiguraGeometrica
    {
        private readonly double _b;
        private readonly double _a;

        public Rectangulo(double b, double a)
        {
            _b = b;
            _a = a;
        }

        public double CalculaPerimetro()
        {
            return (2 * _b) + (2 * _a);
        }

        public double CalculaArea()
        {
            return _b * _a;
        }
    }
}
