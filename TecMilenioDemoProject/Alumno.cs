﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecMilenioDemoProject
{
    public class Triangulo
    {
        private readonly double _c;
        private readonly double _b;
        private readonly double _a;
        private readonly double _h;

        public Triangulo(double a, double b, double c)
        {
            _b = b;
            _a = a;
            _c = c;
            _h = Math.Sqrt(Math.Pow(_b, 2) + Math.Pow(_a, 2));
        }

        public double CalculaArea()
        {
            return (_b * _h) / 2;
        }
    }
}
